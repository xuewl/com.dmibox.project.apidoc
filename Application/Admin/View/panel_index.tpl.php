﻿{:template('_header')}
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/main.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/grid.css" media="all">
<section class="larry-wrapper">
    <!-- overview -->
    <div class="row state-overview">
        <div class="col-lg-2 col-sm-6">
            <section class="panel">
                <div class="symbol kehuguanli"> <i class="iconfont icon-kehuguanli"></i></div>
                <div class="value">
                    <a href="#">10</a>
                    <p>会员总量</p>
                </div>
            </section>
        </div>
        <div class="col-lg-2 col-sm-6">
            <section class="panel">
                <div class="symbol person-add"> <i class="iconfont icon-person-add"></i></div>
                <div class="value">
                    <a href="#">1</a>
                    <p>今日注册</p>
                </div>
            </section>
        </div>
        <div class="col-lg-2 col-sm-6">
            <section class="panel">
                <div class="symbol caifub"><i class="iconfont icon-caifub"></i></div>
                <div class="value">
                    <a href="#">50.00</a>
                    <p>今日缴费</p>
                </div>
            </section>
        </div>
        <div class="col-lg-2 col-sm-6">
            <section class="panel">
                <div class="symbol huiyuandaishenhe"><i class="iconfont icon-huiyuandaishenhe"></i></div>
                <div class="value">
                    <a href="#">0</a>
                    <p>待审认证会员</p>
                </div>
            </section>
        </div>
        <div class="col-lg-2 col-sm-6">
            <section class="panel">
                <div class="symbol xiaofeijifen1"><i class="iconfont icon-xiaofeijifen"></i></div>
                <div class="value">
                    <a href="#">864.26</a>
                    <p>本月奖励总数</p>
                </div>
            </section>
        </div>
        <div class="col-lg-2 col-sm-6">
            <section class="panel">
                <div class="symbol xiaofeijifen2"><i class="iconfont icon-xiaofeijifen"></i></div>
                <div class="value">
                    <a href="#">1358.59</a>
                    <p>累积奖励总数</p>
                </div>
            </section>
        </div>
    </div>
    

    <!-- overview end -->
    <div class="row">
        <div class="col-lg-6">
            <!-- 网站信息统计｛SEO数据统计｝ -->
            <section class="panel">
                <header class="panel-heading bm0">
                    <span class='span-title'>网站信息统计｛SEO数据统计｝</span>
                    <span class="tools pull-right"><a href="javascript:;" class="icon iconfont icon-arrow_down"></a></span>
                </header>
                <div class="panel-body laery-seo-box">
                    <div class="larry-seo-stats" id="larry-seo-stats"></div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading bm0">
                    <span class='span-title'>系统概览</span>
                    <span class="tools pull-right"><a href="javascript:;" class="icon iconfont icon-arrow_down"></a></span>
                </header>
                <div class="panel-body" >
                    <table class="layui-table system" lay-skin="line">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>版本信息</strong>： 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>开发作者</strong>： Larry

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>网站域名</strong>：未定义
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>网站目录</strong>：未定义
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>服务器IP</strong>：未定义
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>服务器环境</strong>：未定义
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>数据库版本</strong>： 未定义

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>最大上传限制</strong>： 未定义

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>当前登录用户</strong>： <span class="current_user">未定义</span>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        <div class="col-lg-6">
            <!-- 系统公告 -->
            <section class="panel">
                <header class="panel-heading bm0">
                    <span class='span-title'>系统公告</span>
                    <span class="tools pull-right"><a href="javascript:;" class="icon iconfont icon-arrow_down"></a></span>
                </header>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <p class="larry_github">感谢耐心等待~</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </section>

            <!-- 最新文章 -->
            <section class="panel">
                <header class="panel-heading bm0">
                    <span class='span-title'>最新会员</span>
                    <span class="badge" style="background-color:#FF3333;color:#FFF"> new </span>
                    <span class="tools pull-right"><a href="javascript:;" class="icon iconfont icon-arrow_down"></a></span>
                </header>
                <div class="panel-body">
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th width="120">用户名</th>
                            <th>邮箱</th>
                            <th>手机</th>
                            <th width="150">时间</th>
                        </tr> 
                        </thead>                    
                        <tbody>
                        <?php foreach ($members as $key => $val): ?>
                            <tr>
                                <td>{$val[mid]}</td>
                                <td>{$val[username]}</td>
                                <td>{$val[email] ? $val[email] : '-'}</td>
                                <td>{$val[mobile] ? $val[mobile] : '-'}</td>
                                <td>{:date('Y/m/d H:i', $val['join_time'])}</td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

</section>

<script type="text/javascript">
    layui.use(['jquery', 'layer', 'element'], function () {
        window.jQuery = window.$ = layui.jquery;
        window.layer = layui.layer;
        window.element = layui.element();

        $('.panel .tools .icon-arrow_down').click(function () {
            var el = $(this).parents(".panel").children(".panel-body");
            if ($(this).hasClass("icon-arrow_down")) {
                $(this).removeClass("icon-arrow_down").addClass("icon-arrow_up");
                el.slideUp(200);
            } else {
                $(this).removeClass("icon-arrow_up").addClass("icon-arrow_down");
                el.slideDown(200);
            }
        })

    });
</script>
<script type="text/javascript" src="<?php echo SKIN_PATH ?>/admin/js/echarts.min.js"></script>
<script type="text/javascript" src="<?php echo SKIN_PATH ?>/admin/js/main.js"></script>
</body>
</html>