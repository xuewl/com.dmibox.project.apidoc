﻿{:template('_header', 'admin')}
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/panel.css" media="all">
<section class="layui-larry-box">
    <div class="larry-personal">
        <div class="layui-tab">
            <blockquote class="layui-elem-quote mylog-info-tit">
            <a href="{:U('add')}" class="layui-btn">添加新成员</a>
            </blockquote>
            <div class="larry-separate"></div>
            <div class="layui-tab-content larry-personal-body clearfix mylog-info-box">
                <!-- 操作日志 -->
                <div class="layui-tab-item layui-field-box layui-show">
                    <table class="layui-table table-hover">
                        <thead>
                            <tr>
                                <th width="100">ID</th>
                                <th>登录名</th>
                                <th>角色</th>
                                <th>手机</th>
                                <th>邮箱</th>
                                <th>添加时间</th>
                                <th>状态</th>
                                <th>登录次数</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($admins as $key => $val): ?>
                            <tr>
                                <td>{$val[id]}</td>
                                <td>{$val[username]}</td>
                                <td>{$val[__group__][name]}</td>                                
                                <td>{$val[mobile]}</td>
                                <td>{$val[email]}</td>
                                <td><?php echo date('Y/m/d H:i', $val['add_time']) ?></td>
                                <td>{$val[status]  ? '启用' : '禁用'}</td>
                                <td>{$val[login_num]}</td>
                                <td>
                                    <a href="{:U('edit', array('id' => $val['id']))}">修改</a>
                                    <a href="{:U('delete', array('id[]' => $val['id']))}">删除</a>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <div class="larry-table-page clearfix">
                        <div id="page" class="page"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>