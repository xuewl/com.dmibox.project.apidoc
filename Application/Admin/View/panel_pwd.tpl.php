{:template('_header')}
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/panel.css" media="all">
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>修改密码</span>
		</header><!-- /header -->
		<div class="larry-personal-body clearfix">
			<form class="layui-form col-lg-5" action="{:U('pwd')}" name="form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">用户名</label>
					<div class="layui-input-block">  
						<input type="text" name="username" autocomplete="off" class="layui-input layui-disabled" value="{$admin[username]}" disabled="disabled" >
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">旧密码</label>
					<div class="layui-input-block">
						<input type="password" name="oldpwd" autocomplete="off" class="layui-input" placeholder="请输入当前密码" lay-verify="required">
					</div>
				</div>


				<div class="layui-form-item">
					<label class="layui-form-label">新密码</label>
					<div class="layui-input-block">
						<input type="password" name="password" autocomplete="off" class="layui-input" placeholder="请输入新的密码" lay-verify="required">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">确认密码</label>
					<div class="layui-input-block">
						<input type="password" name="pwdconfirm" autocomplete="off" class="layui-input" placeholder="请确认新的密码" lay-verify="required">
					</div>
				</div>
				
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<script type="text/javascript">
	layui.use(['form','upload'],function(){
         var form = layui.form();
         layui.upload({ 
             url: '{:U("Api/Upload/upload", array('path' => 'admin', 'ajax' => '1'))}',
             unwrap:false,
             ext: 'jpg|png|gif',
             success: function(ret){
             	if(ret.status == 0) {
             		layer.msg(ret.info);
             		return false;
             	} else {
             		$("input[name='avatar']").attr("value", ret.info.avatar.url);
             	}
            } 
         })
	})
</script>
</body>
</html>