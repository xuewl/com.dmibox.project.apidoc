{:template('_header')}
        <link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/adminstyle.css" media="all">
        <div class="layui-layout layui-layout-admin" id="layui_layout">
            <!-- 顶部区域 -->
            <div class="layui-header header header-demo">
                <div class="layui-main">
                    <!-- logo区域 -->
                    <div class="admin-logo-box">
                        <a class="logo" href="__ROOT__/admin.php" title="logo"><img src="<?php echo SKIN_PATH ?>/admin/images/logo.png" alt=""></a>
                        <div class="larry-side-menu">
                            <i class="iconfont icon-list" aria-hidden="true"></i>
                        </div>
                    </div>
                    <!-- 顶级菜单区域 -->
<!--                     <div class="layui-larry-menu">
                        <ul class="layui-nav clearfix">
                            <li class="layui-nav-item layui-this">
                                <a href="javascirpt:;"><i class="iconfont icon-wangzhanguanli"></i>内容管理</a>
                            </li>
                            <li class="layui-nav-item">
                                <a href="javascirpt:;"><i class="iconfont icon-weixin3"></i>微信公众</a>
                            </li>
                            <li class="layui-nav-item">
                                <a href="javascirpt:;"><i class="iconfont icon-ht_expand"></i>扩展模块</a>
                            </li>
                        </ul>
                    </div> -->
                    <!-- 右侧导航 -->
                    <ul class="layui-nav larry-header-item">
                        <li class="layui-nav-item first">
                            <a href="javascript:;">
                                <img src="<?php echo SKIN_PATH ?>/admin/images/user.jpg" class="userimg">
                                <cite>默认站点</cite>
                                <span class="layui-nav-more"></span>
                            </a>
                            <dl class="layui-nav-child">
                                <dd>
                                    <a href="">站点1</a>
                                </dd>
                                <dd>
                                    <a href="">站点2</a>
                                </dd>

                            </dl>
                        </li>
                        <li class="layui-nav-item">
                            <a href="__APP__" target="_blank" id="lock"><i class="iconfont icon-shouye"></i> 前台</a>
                        </li>
                        <li class="layui-nav-item">
                            <a href="{:U('Public/logout')}"><i class="iconfont icon-logout"></i> 退出</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- 左侧侧边导航开始 -->
            <div class="layui-side layui-side-bg layui-larry-side" id="larry-side">
                <div class="layui-side-scroll" id="larry-nav-side" lay-filter="side">
                    <div class="user-photo">
                        <a class="img" title="我的头像" >
                            <img src="<?php echo SKIN_PATH ?>/admin/images/user.jpg" class="userimg1"></a>
                        <p>你好！{$admin[username]}, 欢迎登录</p>
                    </div>
                    <!-- 左侧菜单 -->
                    <ul class="layui-nav layui-nav-tree">
                        <li class="layui-nav-item layui-this">
                            <a href="javascript:;" data-url="{:U('Panel/index')}">
                                <i class="icon iconfont icon-shouye" data-icon='icon-home1'></i>
                                <span>后台首页</span>
                            </a>
                        </li>
                        <!-- 个人信息 -->
                        <li class="layui-nav-item">
                            <a href="javascript:;">
                                <i class="icon iconfont icon-gongzuopingtai" ></i>
                                <span>我的面板</span>
                                <em class="layui-nav-more"></em>
                            </a>
                            <dl class="layui-nav-child">
                                <dd>
                                    <a href="javascript:;" data-url="{:U('Panel/info')}">
                                        <i class="iconfont icon-tishi" data-icon='icon-geren1'></i>
                                        <span>个人信息</span>
                                    </a>
                                </dd>
                                <dd>
                                    <a href="javascript:;" data-url="{:U('Panel/pwd')}">
                                        <i class="iconfont icon-xiugaimima" data-icon='icon-iconfuzhi01'></i>
                                        <span>修改密码</span>
                                    </a>
                                </dd>
                            </dl>
                        </li>
                        <!-- 用户管理 -->
                        <?php foreach ($init['access'] AS $key => $value): ?>
                        <li class="layui-nav-item">
                            <a href="javascript:;">
                                <i class="icon iconfont icon-{$value[icon]}" ></i>
                                <span>{$value[name]}</span>
                                <em class="layui-nav-more"></em>
                            </a>
                            <dl class="layui-nav-child">
                            	<?php foreach ($value['_child'] AS $cid => $val): ?>
                                <dd>
                                    <a href="javascript:;" data-url="{$val[url]}">
                                        <i class="icon iconfont icon-{$val[icon]}" data-icon='icon-{$val[icon]}'></i>
                                        <span>{$val[name]}</span>
                                    </a>
                                </dd>
                                <?php endforeach ?>
                            </dl>
                        </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>

            <!-- 左侧侧边导航结束 -->
            <!-- 右侧主体内容 -->
            <div class="layui-body" id="larry-body" style="bottom: 0;border-left: solid 2px #1AA094;">
                <div class="layui-tab layui-tab-card larry-tab-box" id="larry-tab" lay-filter="demo" lay-allowclose="true">
                    <ul class="layui-tab-title">
                        <li class="layui-this" id="admin-home"><i class="iconfont icon-diannao1"></i><em>后台首页</em></li>
                    </ul>
                    <div class="layui-tab-content" style="min-height: 150px; ">
                        <div class="layui-tab-item layui-show">
                            <iframe class="larry-iframe" data-id='0' src="{:U('Panel/index')}"></iframe>
                        </div>
                    </div>
                </div>   
            </div>
            <!-- 底部区域 -->
            <div class="layui-footer layui-larry-foot" id="larry-footer">
                <div class="layui-mian">
                    <p class="p-admin">
                        <span>2017 &copy;</span>,<a href="http://www.haidao.la">dmibox</a>. 版权所有
                    </p>
                </div>
            </div>
        </div>
        <!-- 加载js文件-->
        <script type="text/javascript" src="<?php echo SKIN_PATH ?>/admin/js/index.js"></script>
        <!-- 锁屏 -->
<!--         <div class="lock-screen" style="display: none;">
            <div id="locker" class="lock-wrapper">
                <div id="time"></div>
                <div class="lock-box center">
                    <img src="images/userimg.jpg" alt="">
                    <h1>admin</h1>
                    <duv class="form-group col-lg-12">
                        <input type="password" placeholder='锁屏状态，请输入密码解锁' id="lock_password" class="form-control lock-input" autofocus name="lock_password">
                        <button id="unlock" class="btn btn-lock">解锁</button>
                    </duv>
                </div>
            </div>
        </div> -->
        <!-- 菜单控件 -->
        <!-- <div class="larry-tab-menu">
            <span class="layui-btn larry-test">刷新</span>
        </div> -->
        <!-- iframe框架刷新操作 -->
        <!-- <div id="refresh_iframe" class="layui-btn refresh_iframe">刷新</div> -->
    </body>
</html>