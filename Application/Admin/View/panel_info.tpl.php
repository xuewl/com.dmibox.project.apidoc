{:template('_header')}
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/panel.css" media="all">
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>个人信息</span>
		</header><!-- /header -->
		<div class="larry-personal-body clearfix">
			<form class="layui-form col-lg-5" action="{:U('info')}" name="form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">用户名</label>
					<div class="layui-input-block">  
						<input type="text" name="username" autocomplete="off" class="layui-input layui-disabled" value="{$admin[username]}" disabled="disabled" >
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">所属角色</label>
					<div class="layui-input-block">
						<input type="text" name="group_name" value="{$admin[__group__][name]}" autocomplete="off" class="layui-input layui-disabled" disabled="disabled">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">电子邮箱</label>
					<div class="layui-input-block">
						<input type="text" name="email" value="{$admin[email]}" autocomplete="off" class="layui-input" value="Larry">
					</div>
				</div>


				<div class="layui-form-item">
					<label class="layui-form-label">手机号码</label>
					<div class="layui-input-block">
						<input type="text" name="mobile" value="{$admin[mobile]}" autocomplete="off" class="layui-input" placeholder="输入手机号码">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">修改头像</label>
					<div class="layui-input-block">
						<input type="file" class="layui-upload-file" lay-type="images">
					</div>
					<input type="hidden" name="avatar">
				</div>
				
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<script type="text/javascript">
	layui.use(['form','upload'],function(){
         var form = layui.form();
         layui.upload({ 
             url: '{:U("Api/Upload/upload", array('path' => 'admin', 'ajax' => '1'))}',
             unwrap:false,
             ext: 'jpg|png|gif',
             success: function(ret){
             	if(ret.status == 0) {
             		layer.msg(ret.info);
             		return false;
             	} else {
             		$("input[name='avatar']").attr("value", ret.info.avatar.url);
             	}
            } 
         })
	})
</script>
</body>
</html>