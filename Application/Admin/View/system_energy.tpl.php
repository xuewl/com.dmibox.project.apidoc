{:template('_header')}
<style>
.layui-form .layui-form-item label.layui-form-label{
	width: 200px;
}
.layui-input-block {
	margin-left: 200px;
}
</style>
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>能量指数设置</span>
		</header>
		<div class="larry-personal-body clearfix">
			<form class="layui-form" action="" name="myform" method="post">
<table class="layui-table" lay-skin="line">
	<colgroup>
		<col width="200">
		<col>
		<col width="100">
	</colgroup>
	<thead>
		<tr>
			<th>名称</th>
			<th>指数</th>
			<th>操作</th>
		</tr> 
	</thead>
	<tbody>
		<?php foreach ($system['energy'] as $key => $val): ?>
		<tr>
			<td><input type="text" name="energy[name][]" value="{$key}" required lay-verify="required" placeholder="请输入地区名称" autocomplete="off" class="layui-input"></td>
			<td><input type="text" name="energy[quotient][]" value="{$val}" required lay-verify="required" placeholder="请输入地区名称" autocomplete="off" class="layui-input"></td>
			<th><i class="layui-icon" style="color:#FF5722;cursor:pointer" data-event="delete">&#xe640;</i></th>

		</tr>
		<?php endforeach ?>
		<tr>
		<td colspan="3"><button type="button" class="layui-btn layui-btn-normal layui-btn-small" data-event="insert">添加</button></td>
		</tr>
	</tbody>
</table>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button type="submit" class="layui-btn" lay-filter="myform">确认</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
</section>
<script type="text/javascript">
	layui.use(['form'],function(){
		var form = layui.form();
	})

$("form[name=myform]").Validform({
	ajaxPost:true,
	callback:function(ret) {
		if(ret.status == 0) {
			layui.layer.msg(ret.info);
			return false;
		} else {
			window.location.href = ret.url;
		}
	}
})


$("[data-event='insert']").click(function(){
	var html = '<tr><td><input type="text" name="energy[name][]" required lay-verify="required" placeholder="请输入地区名称" autocomplete="off" class="layui-input"></td><td><input type="text" name="energy[quotient][]" required lay-verify="required" placeholder="请输入能量指数" autocomplete="off" class="layui-input"></td><td><i class="layui-icon" style="color:#FF5722;cursor:pointer" data-event="delete">&#xe640;</i></td></tr>';
	$(this).parents("tr").before(html);	
})

$(document).on('click', "[data-event='delete']", function(){
	$(this).parents('tr').remove();
})

</script>
</body>
</html>