﻿{:template('_header', 'admin')}
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/panel.css" media="all">
<section class="layui-larry-box">
    <div class="larry-personal">
        <div class="layui-tab">
            <div class="layui-tab-content larry-personal-body clearfix mylog-info-box">
                <!-- 操作日志 -->
                <div class="layui-tab-item layui-field-box layui-show">
                    <table class="layui-table table-hover">
                        <thead>
                            <tr>
                                <th width="200">名称</th>
                                <th width="100">适用范围</th>
                                <th>描述</th>
                                <th width="100">在线支付</th>
                                <th width="100">安装状态</th>
                                <th width="100">启用状态</th>
                                <th width="100">版本</th>
                                <th width="150">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($payments as $key => $val): ?>
                            <tr>
                                <td>{$val[pay_name]}</td>
                                <td>{$val[applie]}</td>
                                <td>{$val[pay_desc]}</td>
                                <td>{$val[isonline] ? '是' : '否'}</td>
                                <td>{$val[install] ? '已安装' : '未安装'}</td>
                                <td>{$val[enabled]  ? '启用' : '禁用'}</td>
                                <td>{$val['version']}</td>
                                <td>
								<?php if ($val['install'] == 1): ?>
									<a href="{:U('enabled', array('pay_code' => $val['pay_code']))}"><?php echo $val['enabled'] == 1 ? '禁用' : '启用' ?></a>
									<a href="{:U('config', array('pay_code' => $val['pay_code']))}">配置</a>
									<a href="{:U('uninstall', array('pay_code' => $val['pay_code']))}">卸载</a>
								<?php else: ?>
									<a href="{:U('config', array('pay_code' => $val['pay_code']))}">安装</a>
								<?php endif ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <div class="larry-table-page clearfix">
                        <a href="javascript:;" class="layui-btn layui-btn-small"><i class="iconfont icon-shanchu1"></i>删除</a>
                        <div id="page" class="page"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>