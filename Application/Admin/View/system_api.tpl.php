{:template('_header')}
<style>
.layui-form .layui-form-item label.layui-form-label{
	width: 200px;
}
.layui-input-block {
	margin-left: 200px;
}
</style>
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>通信接口设置</span>
		</header>
		
		<div class="layui-tab">
		<blockquote class="layui-elem-quote">云端服务是保障短信通知内正常运作的核心依赖，您必须在海盗官网拥有一个合法账号并有充足的短信配额！</blockquote>
		</div>

		<div class="layui-tab-content larry-personal-body clearfix">
			<form class="layui-form" action="" name="myform" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">接口地址</label>
					<div class="layui-input-block">  
						<input type="text" name="api_uri" lay-verify="required" placeholder="请输入服务器地址" value="{$system[api_uri]}" autocomplete="off" class="layui-input">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">appid</label>
					<div class="layui-input-block">
						<input type="text" name="api_appid" lay-verify="required" placeholder="请输入接口地址" autocomplete="off" value="{$system[api_appid]}" class="layui-input">
					</div>
				</div>


				<div class="layui-form-item">
					<label class="layui-form-label">secret</label>
					<div class="layui-input-block">
						<input type="text" name="api_secret" lay-verify="required" placeholder="请输入接口地址" autocomplete="off" value="{$system[api_secret]}" class="layui-input">
					</div>
				</div>

				<div class="layui-form-item">
					<div class="layui-input-block">
						<button type="submit" class="layui-btn" lay-filter="myform">确认</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<script type="text/javascript">
	layui.use(['form'],function(){
		var form = layui.form();
	})

$("form[name=myform]").Validform({
	ajaxPost:true,
	callback:function(ret) {
		if(ret.status == 0) {
			layui.layer.msg(ret.info);
			return false;
		} else {
			window.location.href = ret.url;
		}
	}
})

</script>
</body>
</html>