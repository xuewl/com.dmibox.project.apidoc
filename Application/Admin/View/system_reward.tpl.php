{:template('_header')}
<style>
.layui-form .layui-form-item label.layui-form-label{
	width: 200px;
}
.layui-input-block {
	margin-left: 200px;
}
</style>
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>奖励全局设置</span>
		</header>
		<div class="larry-personal-body clearfix">
			<form class="layui-form" action="" name="form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">特殊商品结算</label>
					<div class="layui-input-inline">  
						<input type="checkbox" name="month_settle" value="{$system[month_settle]}" lay-skin="switch" checked>
					</div>
					<div class="layui-form-mid layui-word-aux">标识为特殊商品的订单将会在本月结算</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">创业功勋奖说明</label>
					<div class="layui-input-block">
						<textarea name="entr_feats_text" placeholder="请输入内容" class="layui-textarea">{$system[entr_feats_text]}</textarea>
					</div>
				</div>


				<div class="layui-form-item">
					<label class="layui-form-label">个税代缴服务</label>
					<div class="layui-input-inline">  
						<input type="checkbox" name="tax_on" value="{$system[tax_on]}" lay-skin="switch" checked>
					</div>
					<div class="layui-form-mid layui-word-aux">全局个税代缴服务开关</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">个税代缴比例</label>
					<div class="layui-input-inline">  
						<input type="text" name="tax_ratio" value="{$system[tax_ratio]}" class="layui-input">
					</div>
					<div class="layui-form-mid layui-word-aux">发放奖励时会自动按照比例扣除个税同言币</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="demo1">保存设置</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<script type="text/javascript">
	layui.use(['form'],function(){
		var form = layui.form();
	})
</script>
</body>
</html>