<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>同言系统后台管理</title>
        <meta name="renderer" content="webkit">	
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	
        <meta name="apple-mobile-web-app-status-bar-style" content="black">	
        <meta name="apple-mobile-web-app-capable" content="yes">	
        <meta name="format-detection" content="telephone=no">	
        <!-- load css -->
        <link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/layui/css/layui.css" media="all">
        <link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/global.css" media="all">
        <?php if (!$panel_style): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/panel.css" media="all">
        <?php endif ?>
        <link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/iconfont.css" media="all"> 
        <script type="text/javascript" src="<?php echo SKIN_PATH ?>/admin/layui/layui.js"></script>
        <script type="text/javascript">
        var SKIN_PATH = '<?php echo SKIN_PATH ?>';
        </script>
        <script type="text/javascript" src="<?php echo SKIN_PATH ?>/admin/js/use.js"></script>
        <script type="text/javascript" src="<?php echo SKIN_PATH ?>/global/js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="<?php echo SKIN_PATH ?>/global/js/haidao.validate.js"></script>
    </head>
    <body>