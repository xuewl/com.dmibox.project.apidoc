{:template('_header')}
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>添加成员</span>
		</header>
		<div class="larry-personal-body clearfix">
			<form class="layui-form col-lg-5" action="" name="form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">所属角色</label>
					<div class="layui-input-block">  
						<select name="group_id" lay-verify="">				
							<option value="0">请选择</option>
						<?php foreach ($admin_group as $key => $val): ?>
							<option value="{$val[id]}">{$val[name]}</option>
						<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">登录名</label>
					<div class="layui-input-block">  
						<input type="text" name="username" autocomplete="off" class="layui-input" placeholder="请输入登录名">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">登录密码</label>
					<div class="layui-input-block">
						<input type="text" name="password" autocomplete="off" class="layui-input" placeholder="请输入登录密码" lay-verify="required">
					</div>
				</div>


				<div class="layui-form-item">
					<label class="layui-form-label">电子邮件</label>
					<div class="layui-input-block">
						<input type="text" name="email" autocomplete="off" class="layui-input" placeholder="请输入电子邮件" lay-verify="required">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">手机号码</label>
					<div class="layui-input-block">
						<input type="text" name="mobile" autocomplete="off" class="layui-input" placeholder="请输入手机号码" lay-verify="required">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">启用状态</label>
					<div class="layui-input-block">
						<input type="checkbox" name="status" value="1" lay-skin="switch" checked>
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">列表排序</label>
					<div class="layui-input-block">
						<input type="text" name="sort" value="100" class="layui-input" autocomplete="off">
					</div>
				</div>

				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="demo1">确认添加</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<script type="text/javascript">
	layui.use(['form'],function(){
		var form = layui.form();
	})
</script>
</body>
</html>