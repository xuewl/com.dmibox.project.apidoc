﻿{:template('_header')}
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/panel.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo SKIN_PATH ?>/admin/css/grid.css" media="all">
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>添加角色</span>
		</header>

		<div class="larry-personal-body row clearfix">
			<form class="layui-form" name="form" method="post">
			<input type="hidden" name="id" value="{$group[id]}">
			<div class="col-lg-8">			
				<div class="layui-form-item">
					<label class="layui-form-label">名称</label>
					<div class="layui-input-block">  
						<input type="text" name="name" autocomplete="off" placeholder="请输入角色名称" lay-verify="required" class="layui-input" value="{$group[name]}">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">描述</label>
					<div class="layui-input-block">
						<textarea name="description" lay-verify="required" placeholder="请输入" class="layui-textarea">{$group[description]}</textarea>
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">启用状态</label>
					<div class="layui-input-block">
						<input type="checkbox" name="status" value="1" lay-skin="switch" <?php if ($group['status'] == 1): ?>checked<?php endif ?>>
					</div>
				</div>
				
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			
			</div>

			<div class="col-lg-4">
				<fieldset class="layui-elem-field layui-field-title">
					<legend><a name="main">权限配置</a></legend>
				</fieldset>
				<?php foreach ($init['access'] as $key => $val): ?>					
				<ul class="node col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<dt><input type="checkbox" name="rules[]" value="{$val[id]}" lay-filter="rules" title="{$val[name]}" <?php if (in_array($val['id'], $group['rules'])): ?>checked<?php endif ?>></dt>
					<?php if ($val['_child']): ?>
					<dd class="row" style="margin-left:20px;">
						<?php foreach ($val['_child'] as $k => $v): ?>
						<li class="col-lg-4 col-md-3 col-sm-4 col-xs-6"><input type="checkbox" name="rules[]" value="{$v[id]}" lay-filter="rules" title="{$v[name]}" <?php if (in_array($v['id'], $group['rules'])): ?>checked<?php endif ?>></li>
						<?php endforeach ?>
					</dd>
					<?php endif ?>
				</ul>
				<?php endforeach ?>
			</div>

		</form>
		</div>



	</div>
</section>
<script type="text/javascript">
	layui.use(['form','upload'],function(){
		var form = layui.form();
		form.on('checkbox(rules)', function(data){
			$this = $(data.elem);
		  console.log(data.elem.checked);
		  if($this.parent("dt").length == 1) {
		  	$this.parent("dt").siblings("dd").find("input").attr('checked', data.elem.checked);
		  	form.render('checkbox');
		  } else {

		  }
		});
	})
</script>
</body>
</html>