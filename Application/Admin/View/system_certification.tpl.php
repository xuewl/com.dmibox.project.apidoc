{:template('_header')}
<style>
.layui-form .layui-form-item label.layui-form-label{
	width: 200px;
}
.layui-input-block {
	margin-left: 200px;
}
</style>
<section class="layui-larry-box">
	<div class="larry-personal">
		<header class="larry-personal-tit">
			<span>认证相关设置</span>
		</header>
		<div class="larry-personal-body clearfix">
			<form class="layui-form" action="" name="form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">同言币兑换比例</label>
					<div class="layui-input-inline">  
						<input type="text" name="ty_ratio" autocomplete="off" class="layui-input" value="{$system[ty_ratio]}">
					</div>
					<div class="layui-form-mid layui-word-aux">1元人民币可兑换的同言币数量</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">会员认证费用</label>
					<div class="layui-input-inline">
						<input type="text" name="certification_fee" value="{$system[certification_fee]}" class="layui-input" autocomplete="off" lay-verify="required">
					</div>
					<div class="layui-form-mid layui-word-aux">首次认证与续费均采用此价格(人民币价格)</div>
				</div>


				<div class="layui-form-item">
					<label class="layui-form-label">有效血缘层级</label>
					<div class="layui-input-inline">
						<input type="text" name="max_relation_level" value="{$system[max_relation_level]}" autocomplete="off" class="layui-input" lay-verify="required">
					</div>
					<div class="layui-form-mid layui-word-aux">团队奖励及前台显示的最多层级（包含本人所在层级）</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">身份校验KEY</label>
					<div class="layui-input-inline">
						<input type="text" name="identity_key" value="{$system[identity_key]}" autocomplete="off" class="layui-input" lay-verify="required">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">认证条款内容</label>
					<div class="layui-input-block">
						<textarea name="certification_agre" placeholder="请输入内容" class="layui-textarea">{$system[certification_agre]}</textarea>
					</div>
				</div>

				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit="" lay-filter="demo1">确认添加</button>
						<button type="reset" class="layui-btn layui-btn-primary">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<script type="text/javascript">
	layui.use(['form'],function(){
		var form = layui.form();
	})
</script>
</body>
</html>