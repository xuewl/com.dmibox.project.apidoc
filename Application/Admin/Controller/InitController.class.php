<?php
namespace Admin\Controller;
class InitController extends \Common\Controller\InitController
{
	protected $_admin = array();
	protected $_init = array();

	public function _initialize() {
		parent::_initialize();
		C('TMPL_TEMPLATE_SUFFIX', '.tpl.php');
		$this->_admin = D('Admin/Public', 'Service')->init();
		
		if(!$this->_admin['id']) {
			redirect(U('Admin/Public/login'));
		}

		$map = array();
		$map['status'] = 1;
		if($this->_admin['rules']) $map['id'] = array('in', $this->_admin['rules']);
		$access = D('Admin/Node', 'Service')->lists($map, 1000, false);
		$this->_init = array(
			'access' => list_to_tree($access, 'id', 'parent_id'),
		);
		$this->assign('admin', $this->_admin)->assign('init', $this->_init);
	}
}