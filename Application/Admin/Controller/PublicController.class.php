<?php
namespace Admin\Controller;
use \Common\Controller\InitController;
class PublicController extends InitController {
	public function _initialize() {
		parent::_initialize();
        C('TMPL_TEMPLATE_SUFFIX', '.tpl.php');
        $this->service = D('Admin/Public', 'Service');
	}

    public function login(){
    	if(IS_POST) {
    		$post = I('post.');
            $Verify = D('Api/Verify', 'Service');
            if($Verify->check($post['code'], $post['secode']) === false) {
                $this->error($Verify->getError());
            }
            $result = $this->service ->login($post['username'],$post['password'],$post['online']);
            if($result === FALSE) {
                 $this->error($this->service->error);
            } else {
                $this->success('登录成功！', U('Index/index'));
               // redirect(U('Index/Index'));
            }
    	} else {
        	$this->display('login');
    	}
    }
         

     public function logout() {
        $this->service->logout();
        redirect(U('public/login'));
    }
}