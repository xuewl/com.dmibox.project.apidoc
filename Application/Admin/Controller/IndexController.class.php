<?php
namespace Admin\Controller;
use \Admin\Controller\InitController;
class IndexController extends InitController {
	public function _initialize() {
		parent::_initialize();
	}

    public function index(){
        $this->assign('panel_style', 1)->display('index');
    }

    /* 个人面板 */
    public function panel() {

        /* 服务器信息 */
    	$system = array(
    		'name' => @php_uname(),
    		'server_name' => $_SERVER['SERVER_NAME'],
    		'server_addr' => @gethostbyname($_SERVER['SERVER_NAME']),
    	);
        /* 统计数据 */
        $statistics = array(

        );

    	$this->assign('system', $system)
            ->assign('statistics', $statistics)
    	->display();
    }
}