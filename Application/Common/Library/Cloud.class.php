<?php
/**
 *      [Haidao] (C)2013-2099 Dmibox Science and technology co., LTD.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      http://www.haidao.la
 *      tel:400-600-2042
 */
namespace Common\Library;
use \Common\Library\Service;
use \Common\Library\Curl;
class Cloud extends Service
{
    
    protected $_server = 'http://www.haidao.la/api/v2/';
    protected $_params = array();
    protected $_apis = array(
        'member.login' => '/member/account.access_token',
        'sms.send' => '/sms/send',
    );

    public function __construct() {
    	$this->_params = array(
			'format'    => 'json',
			'timestamp' => time(),
    	);
		$this->curl = new Curl();
	} 
    
    /**
     * 处理接口响应
     * @param type $api
     * @param type $method
     */
    public function response($api, $params = array(), $method = 'post') {
        $method = $method == 'post' ? 'postRequest' : 'getRequest';
        $params = array_merge($this->_params, $params);
        $response = $this->curl->$method($this->_server.$this->_apis[$api], $params);
        if(!$response) {
            $this->error = '接口响应失败';
            return false;
        }
        $result = json_decode($response, true);
        if($result['code'] == 200) {
            return $result['result'];
        } else {
            $this->error = $result['msg'];
            return false;
        }
    }
}