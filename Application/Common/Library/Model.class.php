<?php
namespace Common\Library;
class Model extends \Think\Model {

	/* 添加&更新记录 */
	public function update($data, $iscreate = TRUE) {
		if ($iscreate == TRUE) $data = $this->create($data);
		if (empty($data)) {
			$this->error = $this->getError();
			return false;
		}
		if (isset($data[$this->fields['_pk']]) && !empty($data[$this->fields['_pk']])) {
			$result = $this->save($data);
			if ($result === false) {
				$this->error = '更新数据失败';
				return false;
			}
		} else {
			$result = $this->add($data);
			if ($result === false) {
				$this->error = '添加数据失败';
				return false;
			}
		}
		return $result;
	}
}