<?php
namespace Common\Library; 
class Service
{
	function __construct() {
		if(method_exists($this,'_initialize'))
            $this->_initialize();
	}

	public function getError() {
		return $this->error;
	}
}