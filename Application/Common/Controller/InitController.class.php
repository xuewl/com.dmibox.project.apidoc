<?php
namespace Common\Controller;
use Think\Controller;
class InitController extends Controller
{
	protected $pagesize = 12;
	protected $currpage = false;

	public function _initialize() {
    	defined('SKIN_PATH')     or define('SKIN_PATH',      __ROOT__.'/public/');
    	$this->pagesize = 12;
    	$this->currpage = max(1, (int) $_GET[C('VAR_PAGE')]);
    	$this->assign('currpage', $this->currpage);
	}
}
