<?php
/**
 * 获取模板路径
 * @param type $filename
 * @param type $module
 */
function template($filename, $module) {
    if ($module) {
        $filename = $module . '@' . $filename;
    }
    return \Think\Think::instance('Think\View')->fetch($filename);
}

/**
 * 把返回的数据集转换成Tree
 * @param array $list 要转换的数据集
 * @param string $pid parent标记字段
 * @param string $level level标记字段
 * @return array
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function list_to_tree($list, $pk = 'id', $pid = 'pid', $child = '_child', $root = 0) {
    // 创建Tree
    $tree = array();
    if (is_array($list)) {
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] = & $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] = & $list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent = & $refer[$parentId];
                    $parent[$child][] = & $list[$key];
                }
            }
        }
    }
    return $tree;
}

/**
 * 将list_to_tree的树还原成列表
 * @param  array $tree  原来的树
 * @param  string $child 孩子节点的键
 * @param  string $order 排序显示的键，一般是主键 升序排列
 * @param  array  $list  过渡用的中间数组，
 * @return array		返回排过序的列表数组
 * @author yangweijie <yangweijiester@gmail.com>
 */
function tree_to_list($tree, $child = '_child', $order = 'id', &$list = array()) {
    if (is_array($tree)) {
        foreach ($tree as $key => $value) {
            $reffer = $value;
            if (isset($reffer[$child])) {
                unset($reffer[$child]);
                tree_to_list($value[$child], $child, $order, $list);
            }
            $list[] = $reffer;
        }
        $list = list_sort_by($list, $order, $sortby = 'asc');
    }
    return $list;
}

/**
 * 随机字符串
 * @param int $length 长度
 * @param int $numeric 类型(0：混合；1：纯数字)
 * @return string
 */
function random($length, $numeric = 0) {
    $seed = base_convert(md5(microtime() . $_SERVER['DOCUMENT_ROOT']), 16, $numeric ? 10 : 35);
    $seed = $numeric ? (str_replace('0', '', $seed) . '012340567890') : ($seed . 'zZ' . strtoupper($seed));
    if ($numeric) {
        $hash = '';
    } else {
        $hash = chr(rand(1, 26) + rand(0, 1) * 32 + 64);
        $length--;
    }
    $max = strlen($seed) - 1;
    for ($i = 0; $i < $length; $i++) {
        $hash .= $seed{mt_rand(0, $max)};
    }
    return $hash;
}

function submitcheck($var, $allowget = 0, $seccodecheck = 0, $secqaacheck = 0) {
    if(!I($var)) {
        return FALSE;
    } else {
        return true;
    }
}

/**
 * 多维数组合并（支持多数组）
 * @return array
 */
function array_merge_multi() {
    $args = func_get_args();
    $array = array();
    foreach ($args as $arg) {
        if (is_array($arg)) {
            foreach ($arg as $k => $v) {
                if (is_array($v)) {
                    $array[$k] = isset($array[$k]) ? $array[$k] : array();
                    $array[$k] = array_merge_multi($array[$k], $v);
                } else {
                    $array[$k] = $v;
                }
            }
        }
    }
    return $array;
}

/**
 * 对多位数组进行排序
 * @param $multi_array 数组
 * @param $sort_key需要传入的键名
 * @param $sort排序类型
 */
function multi_array_sort($multi_array, $sort_key, $sort = SORT_DESC) {
    if (is_array($multi_array)) {
        foreach ($multi_array as $row_array) {
            if (is_array($row_array)) {
                $key_array[] = $row_array[$sort_key];
            } else {
                return FALSE;
            }
        }
    } else {
        return FALSE;
    }
    array_multisort($key_array, $sort, $multi_array);
    return $multi_array;
}

/**
 * XML转数组
 * @param string $arr
 * @param boolean $isnormal
 * @return array
 */
function xml2array(&$xml, $isnormal = FALSE) {
    $xml_parser = new \Common\Library\Xml($isnormal);
    $data = $xml_parser->parse($xml);
    $xml_parser->destruct();
    return $data;
}

function return_url($code, $method = 'notify') {
    return (is_ssl() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . __ROOT__ . '/api/trade/api.' . $method . '.' . $code . '.php';
}


function para_filter($parameter) {
    $para = array();
    while (list ($key, $val) = each($parameter)) {
        if ($key == "sign" || $key == "sign_type" || $val == "")
            continue;
        else
            $para[$key] = $parameter[$key];
    }
    return $para;
}

function arg_sort($array) {
    $array = para_filter($array);
    ksort($array);
    reset($array);
    return $array;
}

/**
 * 生成签名结果
 * @param $array要加密的数runhook
 * @param return 签名结果字符串
 */
function build_mysign($sort_array, $security_code, $sign_type = "MD5", $issort = TRUE) {
    if ($issort == TRUE) {
        $sort_array = arg_sort($sort_array);
    }
    $prestr = create_linkstring($sort_array);
    $prestr = $prestr . $security_code;
    $mysgin = md5($prestr);
    return $mysgin;
}


function create_linkstring($array, $encode = FALSE) {
    $arg = "";
    while (list ($key, $val) = each($array)) {
        if ($encode === TRUE)
            $val = urlencode($val);
        $arg.=$key . "=" . $val . "&";
    }
    $arg = substr($arg, 0, count($arg) - 2); //去掉最后一个&字符
    return $arg;
}