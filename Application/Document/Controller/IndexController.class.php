<?php
namespace Document\Controller;
use \Common\Controller\InitController;
class IndexController extends InitController {
	public function _initialize() {
		parent::_initialize();
	}
    public function index(){
    	$this->display('index');
    }
}